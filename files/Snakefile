from tools import *
from raw_reads import raw_reads
import os
import re
import snakemake.utils
import csv

#############
# Wildcards #
#############

#le nom des sample et les id ne doivent pas contenir de "/" cela evite de boucler dans les arborescences
wildcard_constraints:
        sample="[^\/]+",
        id="[^\/]+"

STEPS = config["steps"]
PREPARE_REPORT_OUTPUTS = config["prepare_report_outputs"]
PREPARE_REPORT_SCRIPTS = config["prepare_report_scripts"]
OUTPUTS = config["outputs"]
PARAMS_INFO = config["params_info"]
config = config["params"]

##########
# Inputs #
##########

# raw_inputs function call
raw_reads = raw_reads(config['results_dir'], config['sample_dir'], config['SeOrPe'])
config.update(raw_reads)
SAMPLES = raw_reads['samples']



## get reads (trimmed or raw)
def reads():
    inputs = dict()
    if (config["SeOrPe"] == "SE"):
        inputs["read"] = raw_reads['read']
    elif (config["SeOrPe"] == "PE"):
        inputs["read"] = raw_reads['read']
        inputs["read2"] = raw_reads['read2']
    return inputs

def get_individus():
    if (config["demultiplexing"] == "null"):
        return SAMPLES
    else:
        with open(config["process_radtags_barcode_file"], mode="r") as infile:
            reader = csv.reader(infile,delimiter='\t')
            return [row[-1] for row in reader]

individus = get_individus()
# Tools inputs functions

def fastqc_SE_inputs():
	inputs = dict()
	inputs["read"] = raw_reads["read"]
	return inputs

def fastqc_PE_inputs():
	inputs = dict()
	inputs["read"] = raw_reads["read"]
	inputs["read2"] = raw_reads["read2"]
	return inputs

def process_radtags_inputs():
    return reads()

def ustacks_inputs():
    if (config["demultiplexing"] == "null"):
        return reads()
    else:
        inputs = dict()
        if (config["SeOrPe"] == "PE"):
            inputs["read"] = rules.merge_process_radtags_PE.output.read.replace("{id}","{sample}") # Replace wildcard name for compatibility with other tools
            inputs["read2"] = rules.merge_process_radtags_PE.output.read2.replace("{id}","{sample}")
        elif (config["SeOrPe"] == "SE"):
            inputs["read"] = rules.merge_process_radtags_SE.output.read.replace("{id}","{sample}")
        return inputs

def cstacks_inputs():
    inputs = dict()
    inputs["tags"] = expand(rules.ustacks.output.tags, sample=individus)
    inputs["snps"] = expand(rules.ustacks.output.snps, sample=individus)
    inputs["alleles"] = expand(rules.ustacks.output.alleles, sample=individus)
    return inputs

def sstacks_inputs():
    inputs = dict()
    inputs["tags"] = rules.cstacks.output.tags
    inputs["snps"] = rules.cstacks.output.snps
    inputs["alleles"] = rules.cstacks.output.alleles
    return inputs

def tsv2bam_inputs():
    inputs = dict()
    inputs["tsv"] = rules.sstacks.output.matches
    return inputs

def gstacks_inputs():
    inputs = dict()
    inputs["bams"] = expand(rules.tsv2bam.output.bam,sample=individus)
    return inputs

def populations_inputs():
    inputs = dict()
    inputs["catalog_calls"] = rules.gstacks.output.catalog_calls
    inputs["catalog_fa"] = rules.gstacks.output.catalog_fa
    inputs["stats"] = rules.gstacks.output.stats
    return inputs

def prepare_report_inputs():
    inputs = list()
    for step in STEPS:
        inputs.extend(step_outputs(step["name"]))
    return inputs

def prepare_report_scripts():
    scripts = list()
    for step in STEPS:
        tool = config[step["name"]]
        script = tool+".prepare.report.R"
        if (script in PREPARE_REPORT_SCRIPTS):
            scripts.append("/workflow/scripts/"+script)
    return scripts

def prepare_report_outputs():
    outputs = list()
    outputs.append(config["results_dir"] + "/outputs_mqc.csv")
    for step in STEPS:
        tool = config[step["name"]]
        if (tool in PREPARE_REPORT_OUTPUTS.keys()):
            for output in PREPARE_REPORT_OUTPUTS[tool]:
                outputs.append(config["results_dir"]+"/"+tool+"/"+output)
    return outputs

def multiqc_inputs():
    # Need prepare_report inputs and outputs in case prepare_reports has no outputs
    return prepare_report_outputs()

###########
# Outputs #
###########

def step_outputs(step):
    outputs = list()
    if (step == "quality_check"):
        if (config[step] == "fastqc"):
            if(config["SeOrPe"] == "PE"):
                outputs = expand(rules.fastqc_PE.output,sample=SAMPLES)
            else:
                outputs = expand(rules.fastqc_SE.output,sample=SAMPLES)

    elif (step == "demultiplexing"):
        if (config[step] == "process_radtags"):
            if(config["SeOrPe"] == "PE"):
                outputs = expand(rules.merge_process_radtags_PE.output,id=individus)
            else:
                outputs = expand(rules.merge_process_radtags_SE.output,id=individus)
        elif (config[step] == "null"):
            if(config["SeOrPe"] == "PE"):
                outputs = list(expand(raw_reads["read"],sample=individus))
                outputs.extend(expand(raw_reads["read2"],sample=individus))
            else:
                outputs = expand(raw_reads["read"],sample=individus)
            
    elif (step == "ustacks"):
        outputs.extend(expand(rules.ustacks.output.tags,sample=individus))
        outputs.extend(expand(rules.ustacks.output.snps,sample=individus))
        outputs.extend(expand(rules.ustacks.output.alleles,sample=individus))
    
    elif (step == "cstacks"):
        outputs.append(rules.cstacks.output.tags)
        outputs.append(rules.cstacks.output.snps)
        outputs.append(rules.cstacks.output.alleles)

    elif (step == "sstacks"):
        outputs.extend(expand(rules.sstacks.output.matches,sample=individus))

    elif (step == "tsv2bam"):
        outputs.extend(expand(rules.tsv2bam.output.bam,sample=individus))
    
    elif (step == "gstacks"):
        if (config[step] == "gstacks"):
            outputs = list(rules.gstacks.output)

    elif (step == "populations"):
        if (config[step] == "populations"):
            outputs = list(rules.populations.output)

    elif (step == "all"):
        outputs = list(rules.multiqc.output)

    return outputs

# get outputs for each choosen tools
def workflow_outputs(step):
    outputs = list()
    outputs.extend(step_outputs(step))
    return outputs

#########
# Rules #
#########

rule fastqc_SE:
    input:
        **fastqc_SE_inputs()
    output:
        html = config["results_dir"]+'/'+config["fastqc_SE_output_dir"]+'/{sample}'+config["sample_suffix"].split(".")[0]+'_fastqc.html',
        zip = config["results_dir"]+'/'+config["fastqc_SE_output_dir"]+'/{sample}'+config["sample_suffix"].split(".")[0]+'_fastqc.zip',
    log: config["results_dir"]+'/logs/fastqc/{sample}_fastqc_log.txt'
    threads: 1
    params:
        output_dir = config["results_dir"]+'/'+config["fastqc_SE_output_dir"]
    shell:
        "fastqc "
        "{input.read} "
        "-t {threads} "
        "-o {params.output_dir} "
        "--quiet "
        "|& tee {log}"

rule fastqc_PE:
    input:
        **fastqc_PE_inputs()
    output:
        html1 = config["results_dir"]+'/'+config["fastqc_PE_output_dir"]+'/{sample}_R1'+config["sample_suffix"].split(".")[0]+'_fastqc.html',
        zip1 = config["results_dir"]+'/'+config["fastqc_PE_output_dir"]+'/{sample}_R1'+config["sample_suffix"].split(".")[0]+'_fastqc.zip',
        html2 = config["results_dir"]+'/'+config["fastqc_PE_output_dir"]+'/{sample}_R2'+config["sample_suffix"].split(".")[0]+'_fastqc.html',
        zip2 = config["results_dir"]+'/'+config["fastqc_PE_output_dir"]+'/{sample}_R2'+config["sample_suffix"].split(".")[0]+'_fastqc.zip',
    log: config["results_dir"]+'/logs/fastqc/{sample}_fastqc_log.txt'
    threads: 2
    params:
        output_dir = config["results_dir"]+'/'+config["fastqc_PE_output_dir"]
    shell:
        "fastqc "
        "{input.read} "
        "{input.read2} "
        "-t {threads} "
        "-o {params.output_dir} "
        "--quiet "
        "|& tee {log}"

ruleorder:  fastqc_PE > fastqc_SE



rule process_radtags_SE:
    input:
        **process_radtags_inputs(),
        barcode_file = config["process_radtags_barcode_file"]
    output:
        temp(expand(config["results_dir"]+"/"+config["process_radtags_SE_output_dir"]+"/{{sample}}"+"/{id}"+config["sample_suffix"],id=individus)) # fastq par individu pour un sample
    params:
        output_dir = config["results_dir"]+"/"+config["process_radtags_SE_output_dir"]+"/{sample}",
        reads_dir = lambda w, input: os.path.dirname(input.read[0]),
        process_radtags_barcode_type = config["process_radtags_barcode_type"],
        process_radtags_enzyme_SE = config["process_radtags_enzyme_SE"]
    log:
        config["results_dir"]+"/logs/process_radtags_{sample}_log.txt"
    shell:
        "process_radtags "
        "-f {input.read} "
        "-b {input.barcode_file} "
        "-o {params.output_dir} "
        "{params.process_radtags_barcode_type} "
        "-e {params.process_radtags_enzyme_SE} "
        "--clean "
        #"--quality "
        "--rescue "
        "|& tee {log}"

rule merge_process_radtags_SE:
    input:
        read = expand(config["results_dir"]+"/"+config["process_radtags_SE_output_dir"]+"/{sample}/{{id}}"+config["sample_suffix"],sample=SAMPLES)
    output:
        read = config["results_dir"]+"/"+config["process_radtags_SE_output_dir"]+"/{id}"+config["sample_suffix"]
    run:
        fqs = ' '.join(input.read)
        out = "{output.read}"
        shell("cat "+fqs+" > "+out)

rule process_radtags_PE:
    input:
        **process_radtags_inputs(),
        barcode_file = config["process_radtags_barcode_file"]
    output:
        temp(expand(config["results_dir"]+"/"+config["process_radtags_PE_output_dir"]+"/{{sample}}"+"/{id}.1.fq.gz",id=individus)), # fastq par individu pour un sample
        temp(expand(config["results_dir"]+"/"+config["process_radtags_PE_output_dir"]+"/{{sample}}"+"/{id}.2.fq.gz",id=individus)) # fastq par individu pour un sample
    params:
        output_dir = config["results_dir"]+"/"+config["process_radtags_PE_output_dir"]+"/{sample}",
        reads_dir = lambda w, input: os.path.dirname(input.read[0]),
        process_radtags_barcode_type = config["process_radtags_barcode_type"],
        process_radtags_enzyme_1_PE = "--renz_1 " + config["process_radtags_enzyme_1_PE"],
        process_radtags_enzyme_2_PE = "" if (config["process_radtags_enzyme_2_PE"] == "") else "--renz_2 " + config["process_radtags_enzyme_2_PE"]
    log:
        config["results_dir"]+"/logs/process_radtags_{sample}_log.txt"
    shell:
        "process_radtags "
        "-1 {input.read} "
        "-2 {input.read2} "
        "-b {input.barcode_file} "
        "-o {params.output_dir} "
        "{params.process_radtags_barcode_type} "
        "{params.process_radtags_enzyme_1_PE} "
        "{params.process_radtags_enzyme_2_PE} "
        "--clean "
        #"--quality "
        "--rescue "
        "|& tee {log}"

rule merge_process_radtags_PE:
    input:
        read = expand(config["results_dir"]+"/"+config["process_radtags_PE_output_dir"]+"/{sample}/{{id}}.1.fq.gz",sample=SAMPLES),
        read2 = expand(config["results_dir"]+"/"+config["process_radtags_PE_output_dir"]+"/{sample}/{{id}}.2.fq.gz",sample=SAMPLES)
    output:
        read = config["results_dir"]+"/"+config["process_radtags_PE_output_dir"]+"/{id}_R1"+config["sample_suffix"],
        read2 = config["results_dir"]+"/"+config["process_radtags_PE_output_dir"]+"/{id}_R2"+config["sample_suffix"]
    run:
        fq1 = ' '.join(input.read)
        fq2 = ' '.join(input.read2)
        out1 = "{output.read}"
        out2 = "{output.read2}"
        shell("cat "+fq1+" > "+out1)
        shell("cat "+fq2+" > "+out2)

ruleorder:  process_radtags_PE > process_radtags_SE
ruleorder:  merge_process_radtags_PE > merge_process_radtags_SE



def rename_ustacks_outputs():
    command = ""
    if (config["SeOrPe"]=="PE"):
        prefix_in = config["results_dir"]+"/"+config["ustacks_output_dir"]+"/"
        prefix_out = config["results_dir"]+"/"+config["ustacks_output_dir"]+"/"
        command += ' && mv '+prefix_in+'{sample}_R1.tags.tsv.gz '+prefix_out+'{sample}.tags.tsv.gz'
        command += ' && mv '+prefix_in+'{sample}_R1.snps.tsv.gz '+prefix_out+'{sample}.snps.tsv.gz'
        command += ' && mv '+prefix_in+'{sample}_R1.alleles.tsv.gz '+prefix_out+'{sample}.alleles.tsv.gz'
    return command

rule ustacks:
    input:
        **ustacks_inputs(),
    output:
        tags = config["results_dir"]+"/"+config["ustacks_output_dir"]+"/{sample}.tags.tsv.gz",
        snps = config["results_dir"]+"/"+config["ustacks_output_dir"]+"/{sample}.snps.tsv.gz",
        alleles = config["results_dir"]+"/"+config["ustacks_output_dir"]+"/{sample}.alleles.tsv.gz"
    params:
        output_dir = config["results_dir"]+"/"+config["ustacks_output_dir"],
        m = config["ustacks_m"],
        M = config["ustacks_M"],
        N = config["ustacks_N"],
        i = lambda w: individus.index(w.sample),
        move = rename_ustacks_outputs(),
    threads:
        config["ustacks_threads"]
    log:
        config["results_dir"]+"/logs/ustacks/{sample}_ustacks_log.txt"
    shell:
        "ustacks "
        "-f {input.read} "
        "-p {threads} "
        "-i {params.i} "
        "-m {params.m} "
        "-M {params.M} "
        "-N {params.N} "
        "-o {params.output_dir} "
        "|& tee {log}"
        "{params.move}"


rule cstacks:
    input:
        **cstacks_inputs(),
        popmap = config["cstacks_population_tsv"]
    output:
        tags = config["results_dir"]+"/"+config["cstacks_output_dir"]+"/catalog.tags.tsv.gz",
        snps = config["results_dir"]+"/"+config["cstacks_output_dir"]+"/catalog.snps.tsv.gz",
        alleles = config["results_dir"]+"/"+config["cstacks_output_dir"]+"/catalog.alleles.tsv.gz"
    params:
        n = config["cstacks_n"],
        input_dir = config["results_dir"]+"/"+config["ustacks_output_dir"],
        output_dir = config["results_dir"]+"/"+config["cstacks_output_dir"],
    threads: 
        config["cstacks_threads"]
    log:
        config["results_dir"]+"/logs/cstacks/cstacks_log.txt"
    shell:
        "cstacks "
        "-p {threads} "
        "-n {params.n} "
        "-P {params.input_dir} "
        "-M {input.popmap} "
        #"-o {params.output_dir} "
        "|& tee {log}"

rule sstacks:
    input:
        **sstacks_inputs()
    output:
        matches = config["results_dir"]+"/"+config["sstacks_output_dir"]+"/{sample}.matches.tsv.gz"
    params:
        cstacks_dir = config["results_dir"]+"/"+config["cstacks_output_dir"],
        output_dir = config["results_dir"]+"/"+config["sstacks_output_dir"],
        sample = config["results_dir"]+"/"+config["ustacks_output_dir"]+"/{sample}"
    threads: 
        config["sstacks_threads"]
    log:
        config["results_dir"]+"/logs/sstacks/{sample}_sstacks_log.txt"
    shell:
        "sstacks "
        "-p {threads} "
        "-c {params.cstacks_dir} "
        "-s {params.sample} "
        "-o {params.output_dir} "
        "|& tee {log}"

def link_reads_tsv2bam():
    if (config["SeOrPe"]=="PE"):
        if (config["demultiplexing"]=="process_radtags"):
            command = ""
            prefix_in = config["results_dir"]+"/"+config["process_radtags_PE_output_dir"]+"/"
        else:
            command = "mkdir -p "+config["process_radtags_PE_output_dir"]+" && "
            prefix_in = config["sample_dir"]+"/"
        prefix_out = config["results_dir"]+"/"+config["process_radtags_PE_output_dir"]+"/"
        command += 'ln -s '+prefix_in+'{sample}_R1'+config["sample_suffix"]+' '+prefix_out+'{sample}.1.fq.gz && '
        command += 'ln -s '+prefix_in+'{sample}_R2'+config["sample_suffix"]+' '+prefix_out+'{sample}.2.fq.gz && '
    return command

rule tsv2bam:
    input:
        **tsv2bam_inputs()
    output:
        bam = config["results_dir"]+"/"+config["tsv2bam_output_dir"]+"/{sample}.bam"
    params:
        sstacks_dir = config["results_dir"]+"/"+config["sstacks_output_dir"],
        pe_reads_dir = "--pe-reads-dir "+config["results_dir"]+"/"+config["process_radtags_PE_output_dir"] if config["SeOrPe"] == "PE" else "",
        out_name = config["results_dir"]+"/"+config["sstacks_output_dir"]+"/{sample}.matches.bam",
        link_reads = link_reads_tsv2bam()
    threads: 
        config["tsv2bam_threads"]
    log:
        config["results_dir"]+"/logs/tsv2bam/{sample}_tsv2bam_log.txt"
    shell:
        "{params.link_reads} "
        "tsv2bam "
        "-t {threads} "
        "-P {params.sstacks_dir} "
        "-s {wildcards.sample} "
        "{params.pe_reads_dir} "
        "|& tee {log} "
        "&& mv {params.out_name} {output}"

rule gstacks:
    input:
        **gstacks_inputs(),
        popmap = config["gstacks_population_tsv"]
    output:
        catalog_calls = config["results_dir"]+"/"+config["gstacks_output_dir"]+"/catalog.calls",
        catalog_fa = config["results_dir"]+"/"+config["gstacks_output_dir"]+"/catalog.fa.gz",
        stats = config["results_dir"]+"/"+config["gstacks_output_dir"]+"/gstacks.log.distribs"
    params:
        output_dir = config["results_dir"]+"/"+config["gstacks_output_dir"],
        bam_dir = lambda w, input: os.path.dirname(input.bams[0]),
        model = config["gstacks_model"],
        var_alpha = config["gstacks_var_alpha"],
        gt_alpha = config["gstacks_gt_alpha"],
        min_mapq = config["gstacks_min_mapq"],
        max_clipped = config["gstacks_max_clipped"]
    threads:
        config["gstacks_threads"]
    log:
        config["results_dir"]+"/logs/gstacks/gstacks_log.txt"
    shell:
        "gstacks "
        "-t {threads} "
        "-I {params.bam_dir} "
        "-M {input.popmap} "
        "--model {params.model} "
        "--var-alpha {params.var_alpha} "
        "--gt-alpha {params.gt_alpha} "
        "--min-mapq {params.min_mapq} "
        "--max-clipped {params.max_clipped} "
        "-O {params.output_dir} "
        "|& tee {log}"


rule populations:
    input:
        **populations_inputs(),
        popmap = config["gstacks_population_tsv"]
    output:
        log = config["results_dir"]+"/"+config["populations_output_dir"]+"/populations.log"
    params:
        output_dir = config["results_dir"]+"/"+config["populations_output_dir"],
        gstacks_dir = config["results_dir"]+"/"+config["gstacks_output_dir"],
        r = config["populations_r"],
        max_obs_het = config["populations_max_obs_het"],
        min_maf = config["populations_min_maf"],
        p = config["populations_p"]
    threads:
        config["populations_threads"]
    log:
        config["results_dir"]+"/logs/populations_log.txt"
    shell:
        "populations "
        "-t {threads} "
        "-P {params.gstacks_dir} "
        "-M {input.popmap} "
        "-O {params.output_dir} "
        "-r {params.r} "
        "--max-obs-het {params.max_obs_het} "
        "--min-maf {params.min_maf} "
        "-p {params.p} "
        "--vcf "
        "--fstats "
        "--genepop "
        "--structure "
        "--radpainter "
        "--plink "
        "--phylip "
        "|& tee {log} "
        "&& gzip -f {params.output_dir}/populations.snps.vcf"




rule prepare_report:
    input:
        *prepare_report_inputs(),
    output:
        *prepare_report_outputs(),
        config_multiqc = config["results_dir"] + "/config_multiqc.yaml",
        params_tab = config["results_dir"] + "/params_tab_mqc.csv",
        versions_tab = config["results_dir"] + "/Tools_version_mqc.csv"
    params:
        params_file = workflow.overwrite_configfiles,
        results_dir = config["results_dir"]
    log:
        config["results_dir"]+"/logs/prepare_report_log.txt"
    run:
        # Specific scripts for each tool
        for script in prepare_report_scripts():
            shell("Rscript "+script+" {params.params_file} |& tee {log}")
        # Outputs files for Multiqc report
        outfile = config["results_dir"] + "/outputs_mqc.csv"
        head = """
# description: 'This is the list of the files generated by each step of the workflow'
# section_name: 'Workflow outputs'
"""
        with open(outfile,"w") as out:
            out.write(head)
            out.write("step\ttool\tfile\tdescription\n")#\tname
            for step in STEPS:
                tool = config[step["name"]]
                i=1
                for command in OUTPUTS[tool]:
                    if ("SeOrPe" not in config.keys() or (config["SeOrPe"] == "SE" and not("_PE" in command)) or (config["SeOrPe"] == "PE" and not("_SE" in command))):
                        outputs = OUTPUTS[tool][command]
                        for files in outputs:
                            name = files["file"] if 'file' in files.keys() else files["directory"]
                            path = config[command+"_output_dir"] + "/" + name #config["results_dir"] +"/"+
                            out.write(str(i)+"-"+step["title"]+"\t"+tool+"\t"+path+"\t"+files["description"]+"\n")#"\t"+files["name"]+
                            i+=1
        
        # Params list for Multiqc report
        params_list = "params_name\tdescription\tvalue\n"
        head = """# description: 'This is the list of the parameters for each rule'
# section_name: 'Workflow parameters'
"""
        for step in STEPS:
            tool = config[step["name"]]
            for key, value in config.items():    
                if (tool in key and tool != "null") or (key in ["results_dir","sample_dir","sample_suffix","SeOrPe"]) and ("SeOrPe" not in config.keys() or (config["SeOrPe"] == "SE" and not("_PE" in command)) or (config["SeOrPe"] == "PE" and not("_SE" in command))):
                    if (key in PARAMS_INFO.keys() and "label" in PARAMS_INFO[key].keys()):
                        description = PARAMS_INFO[key]["label"]
                    else:
                        description = ''
                    params_list += key + "\t'" + description + "'\t'" + str(value) + "'\n"

        with open(output.params_tab,"w") as out:
            out.write(head)
            out.write(params_list)

        # Tools version
        with open(output.versions_tab,"w") as out:
            versions = read_yaml("/workflow/versions.yaml")
            head = """# description: 'This is the list of the tools used and their version'
# section_name: 'Tools version'
"""
            out.write(head)
            out.write("Tool\tVersion"+"\n")
            for step in STEPS:
                tool = config[step["name"]]
                if (tool in versions.keys()):
                    out.write(tool+"\t"+versions[tool]+"\n")

        # Config for Multiqc report
        shell("python3 /workflow/generate_multiqc_config.py {params.params_file} {output.config_multiqc}")

rule multiqc:
    input:
        multiqc_inputs(),
        config_multiqc = config["results_dir"] + "/config_multiqc.yaml"
    output:
        multiqc_dir = directory(config["results_dir"]+"/multiqc_data")
    params:
        output_dir = config["results_dir"]
    log:
        config["results_dir"]+'/logs/multiqc/multiqc_log.txt'
    shell:
        "multiqc --config {input.config_multiqc} -f {params.output_dir} "
        "-o {params.output_dir} |& tee {log}"

# Final Snakemake rule waiting for outputs of the final step choosen by user (default all steps)
rule all:
    input:
        workflow_outputs("all")
    output:
        Snakefile = config["results_dir"]+"/workflow/Snakefile",
        get_samples = config["results_dir"]+"/workflow/get_samples.py",
        scripts = directory(config["results_dir"]+"/workflow/scripts"),
        params = config["results_dir"]+"/workflow/params.yml"
    params:
        params_file = workflow.overwrite_configfiles,
    shell:
        "cp /workflow/Snakefile {output.Snakefile} && "
        "cp /workflow/get_samples.py {output.get_samples} && "
        "cp -r /workflow/scripts {output.scripts} && "
        "cp {params.params_file} {output.params}"

onsuccess:
    print("Workflow finished with SUCCESS")
    shell("touch "+config["results_dir"]+"/logs/workflow_end.ok")

onerror:
    print("An ERROR occurred")
    shell("cat {log} > "+config["results_dir"]+"/logs/workflow_end.error")
    #shell("mail -s "an error occurred" youremail@provider.com < {log}")

