library(SNPRelate)
library(RColorBrewer)
library(ComplexHeatmap)
library(yaml)

palette = "Accent" #"Spectral"

args = commandArgs(trailingOnly=TRUE)
parameters = read_yaml(args[1])$params

vcf.fn = paste(parameters$results_dir,parameters$populations_output_dir,"populations.snps.vcf.gz",sep = "/")
#vcf.fn = "/results/populations/populations.snps.vcf"
popmap=read.table(parameters$gstacks_population_tsv, header=F, sep='\t', stringsAsFactors=F)
#popmap=read.table("/samples/AgneseRAD/ran_pop_map.txt",header=F, sep='\t', stringsAsFactors=F)

snpgdsVCF2GDS(vcf.fn,"/tmp/batch_1.gds", method="biallelic.only", verbose=FALSE)

genofile <- snpgdsOpen("/tmp/batch_1.gds")

PCA1 <- snpgdsPCA(genofile,  snp.id=NULL, maf=NaN, missing.rate=0.2, num.thread=10, verbose=FALSE, autosome.only=FALSE)

#PCA eigenvalues
fic = paste(parameters$results_dir,parameters$populations_output_dir,'PCA_Eigenvalues_mqc.txt',sep = "/")
    cat("# id: custom_bargraph_tsv
# section_name: 'PCA eigenvalues'
# description: 'valeurs propres (variance expliquée par chaque axe).'
# format: 'tsv'
# plot_type: 'bargraph'
# pconfig:
#    id: 'custom_bargraph_w_header'
#    title: PCA eigenvalues
#    ylab: 'Percent'\n", file=fic)

     for (i in 1: 20 )
     {
        cat(i,'\t',PCA1$eigenval[i],'\n', file=fic, append=T)
     }

sample.id1 <- PCA1$sample.id
pop_code1= popmap[which(popmap[,1] %in% sample.id1), 2]

nbpops = length(unique(pop_code1)) 

if ( nbpops <= 11)   { 
    popc <- brewer.pal(n = nbpops, name = palette)
} else {
    popc <- colorRampPalette(brewer.pal(11, palette))( nbpops)
}

#popc <- brewer.pal(n = length(unique(pop_code1)), name = 'Spectral')

names(popc) = unique(pop_code1)
pop_color1 = popc[pop_code1]
names(pop_color1) = sample.id1

# make a data.frame
tab1 <- data.frame(sample.id = sample.id1,
                   pop = factor(pop_code1),
                   EV1 = PCA1$eigenvect[,1],    # the first eigenvector
                   EV2 = PCA1$eigenvect[,2],    # the second eigenvector
                   stringsAsFactors = FALSE)

#PCA axe 1 et 2
fic = paste(parameters$results_dir,parameters$populations_output_dir,'PCA_First2axis_mqc.yaml',sep = "/")
cat("id: 'PCA'
section_name: 'PCA'
description: 'This plot is generated from some PCA data'
plot_type: 'scatter'
pconfig:
    id: 'mqc_pca_scatter_plot'
    title: 'PCA Plot'
    ylab: 'PC 1'
    xlab: 'PC 2'
data: \n", file=fic)

for (i in 1:length(tab1$sample.id))
{
    cat("    ", i,":\n",file=fic, append=T)
    cat("        x:",tab1$EV2[i],"\n",file=fic, append=T)
    cat("        y:",tab1$EV1[i],"\n",file=fic, append=T)
    cat("        color: '",pop_color1[tab1$sample.id[i]],"'\n",file=fic, append=T)
}


#FST par paire
tryCatch({
    cat("#plot_type: 'heatmap'\n\t",file = paste(parameters$results_dir,parameters$populations_output_dir,'Mean_Pairwise_Pop_FST_mqc.csv',sep = "/"))
    fst_summary = read.csv2(paste(parameters$results_dir,parameters$populations_output_dir,'populations.fst_summary.tsv',sep = "/"), sep = "\t",row.names = 1, stringsAsFactors=F)
    if(nrow(fst_summary) > 0){
        for (i in 1: nrow(fst_summary))
        {
            fst_summary[i,i] = 0
            for (j in 1: i) {fst_summary[i,j] = fst_summary[j,i]}
        }
    }
    write.table(fst_summary, sep='\t',file = paste(parameters$results_dir,parameters$populations_output_dir,'Mean_Pairwise_Pop_FST_mqc.csv',sep = "/"), quote=F, row.names=T, col.names=T ,append=TRUE)
}, error = function(e) {
    cat("",file = paste(parameters$results_dir,parameters$populations_output_dir,'Mean_Pairwise_Pop_FST_mqc.csv',sep = "/"))
})

#IBS
tryCatch({
    ibs <- snpgdsIBS(genofile, sample.id=sample.id1, snp.id=NULL, maf=0, missing.rate=1, num.thread=2, verbose=TRUE, autosome.only=FALSE)
    colnames(ibs$ibs)=ibs$sample.id
    rownames(ibs$ibs)=ibs$sample.id
    png(filename = paste(parameters$results_dir,parameters$populations_output_dir,"IBS_mqc.png",sep = "/"), height=800, width=800)
    topAnnot = HeatmapAnnotation(Population = pop_code1, col = list(Population=popc))
    leftAnnot = HeatmapAnnotation(Population = pop_code1, col = list(Population=popc),which = "row",show_legend = FALSE)
    print(Heatmap(ibs$ibs, top_annotation=topAnnot, left_annotation=leftAnnot))
    dev.off()
}, error = function(e) {
    png(filename = paste(parameters$results_dir,parameters$populations_output_dir,"IBS_mqc.png",sep = "/"), height=800, width=800)
    print(plot(c(0,1),c(0,1),ann =F,bty ='n',type ='n',xaxt ='n',yaxt ='n'))
    text(x =0.5,y =0.5,paste("Problem with IBS "),cex =1.6,col ="black")
    dev.off()
})


#snpgdsClose(genofile)

