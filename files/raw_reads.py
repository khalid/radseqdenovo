import os
import re
import sys

def raw_reads(results_dir, sample_dir, SeOrPe):
    samples = list()
    PE_mark = "" # _R or _
    suffixes = list()
    dicoSamples = dict() # sample_name: file(s)
    files = os.listdir(sample_dir)
    if SeOrPe == "PE":
        regex = re.compile(r"^(.+)(_R1|_R2|_1|_2)(.+)")
    else:
        regex = re.compile(r"^(.+?)(\..*)")
    for file in files:
        res = re.match(regex, file)
        if res:
            if res.group(1) not in samples:
                samples.append(res.group(1))
                if SeOrPe == "PE":
                    suffixes.append(res.group(3))
                    if len(res.group(2)) == 3:
                        PE_mark = "_R"
                    else:
                        PE_mark = "_"
                else:
                    suffixes.append(res.group(2))

            if res.group(1) not in dicoSamples.keys():
                dicoSamples[res.group(1)] = list()

            dicoSamples[res.group(1)].append(file)

    if (len(set(suffixes)) == 1 ):
        suffix = list(set(suffixes))[0]
        with open(results_dir+"/samples.tsv","w") as sampleTab:
            if SeOrPe == "PE":
                sampleTab.write("sample\tfile_read_1\tfile_read_2")
            else:
                sampleTab.write("sample\tfile_read_1")
            for sample in sorted(samples):
                sampleTab.write("\n"+sample+"\t"+"\t".join(sorted(dicoSamples[sample])))

        out = {'samples': sorted(samples), 'sample_suffix': suffix, 'dico': dicoSamples}

        if SeOrPe == "SE":
            out ["read"] = os.path.join(sample_dir,"{sample}"+suffix)
            out ["read2"] = ""
        else:
            out ["read"] = os.path.join(sample_dir,"{sample}"+PE_mark+"1"+suffix)
            out ["read2"] = os.path.join(sample_dir,"{sample}"+PE_mark+"2"+suffix)

        return out
    else:
        exit("Files have different suffixes:" + ','.join(suffixes))

#print(raw_reads(sys.argv[1],sys.argv[2],sys.argv[3]))