import re
import sys
from tools import read_yaml

config = read_yaml(sys.argv[1])

def report_section_order():
    res = "skip_generalstats: true\n\n"
    res += "report_section_order:\n"
    res += "  Rule_graph:\n" 
    res += "    order: 990\n"
    res += "  params_tab:\n" 
    res += "    order: 980\n"
    res += "  outputs:\n" 
    res += "    order: 970\n"
    res += "  Tools_version:\n" 
    res += "    order: 960\n"
    cpt = 950
    for step in config["steps"]:
        tool = config["params"][step["name"]]
        if (config["multiqc"][tool] != "custom"):
            res += "  " + config["multiqc"][tool] + ":\n"
            res += "    " + "order: " + str(cpt) + "\n"
            cpt += -10
        for rule in config["outputs"][tool]:
            if ("SeOrPe" not in config.keys() or (config["params"]["SeOrPe"] == "SE" and not("_PE" in rule)) or (config["params"]["SeOrPe"] == "PE" and not("_SE" in rule))):
                for output in config["outputs"][tool][rule]:
                    if("file" in output.keys() and "mqc" in output["file"] and '{' not in output["file"]): # case of dynamic files ({wildcard}_mqc.png) to deal with
                        section = re.sub('\_mqc.*$', '', output["file"])
                        res += "  " + section + ":\n" 
                        res += "    " + "order: " + str(cpt) + "\n"
                        cpt += -10

    return res

def main():
    res = ""
    res += report_section_order()

    with open(sys.argv[2],"w") as out:
        out.write(res)

if __name__ == "__main__":
    # execute only if run as a script
    main()