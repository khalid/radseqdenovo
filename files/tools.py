import oyaml as yaml
import shutil

def read_yaml(filepath):
    try:
        with open(filepath, 'r') as file:
            data = yaml.load(file, Loader=yaml.FullLoader)
            return data
    except IOError as e:
        print("Error in file opening:", e)
    except yaml.YAMLError as exc:
        print("Error in yaml loading:", exc)

def write_yaml(filepath,data):
    try:
        with open(filepath, 'w') as file:
            yaml.dump(data, file, default_flow_style=False)
    except IOError as e:
        print("Error in file opening:", e)

def copy_dir(src,dst):
    try:
        shutil.copytree(src,dst)
    except FileExistsError:
        shutil.rmtree(dst, ignore_errors=True)
        shutil.copytree(src,dst)