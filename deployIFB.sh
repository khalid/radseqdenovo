#!/bin/bash

# This script is executed on the virtual machine during the *Deployment* phase.
# It is used to apply parameters specific to the current deployment.
# It is executed secondly during a cloud deployement in IFB-Biosphere, after the *Installation* phase.
if [ $# -lt 1 ]
then
    APP_IMG="mbbteam/radseqdenovo:latest"  
else 
    IMG_SRC=$1
    case $IMG_SRC in
        ifb)
            APP_IMG="gitlab-registry.in2p3.fr/ifb-biosphere/apps/radseqdenovo:master" ;;
        docker )
            APP_IMG="mbbteam/radseqdenovo:latest"  ;;
        local)
            docker build . -t radseqdenovo:latest 
            APP_IMG="radseqdenovo:latest" ;;
        mbb)    
            #APP_IMG="X.X.X.X:5000/radseqdenovo:latest" ;;
    esac
fi


# Tuning if site proxy or not
#CLOUD_SERVICE = $(ss-get cloudservice)
#CLOUD_SERVICE="ifb-genouest-genostack"

#HOST_NAME=$( ss-get --timeout=3 hostname )
HOST_NAME="192.168.100.49"
#if [ "$CLOUD_SERVICE" == "ifb-genouest-genostack" ]; then
    # Cloud site WITH a site proxy
#    APP_PORT=80
#    PROXIED_IP=$( echo $HOST_NAME | sed "s|\.|-|g")
#    HOST_NAME="openstack-${PROXIED_IP}.genouest.org"
#    HTTP_ENDP="https://$HOST_NAME"

#    systemctl stop nginx
#else
    # Cloud site WOUT a site proxy
    APP_PORT=8787
    HTTP_ENDP="https://$HOST_NAME"

    openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/ssl/private/nginx-selfsigned.key -out /etc/ssl/certs/nginx-selfsigned.crt -subj "/C=FR/ST=AURA/L=Lyon/O=IFB/OU=IFB-biosphere/CN=myrstudio.biosphere.france-bioinformatique.fr"
    openssl dhparam -out /etc/ssl/certs/dhparam.pem 2048

    mkdir -p /etc/nginx/snippets
    echo "ssl_certificate /etc/ssl/certs/nginx-selfsigned.crt;" > /etc/nginx/snippets/self-signed.conf
    echo "ssl_certificate_key /etc/ssl/private/nginx-selfsigned.key;" >> /etc/nginx/snippets/self-signed.conf

    cp system/nginx_snippets_ssl-params.conf /etc/nginx/snippets/ssl-params.conf

    cp /etc/nginx/sites-available/default /etc/nginx/sites-available/default.bak

    cp system/nginx_sites-available_default /etc/nginx/sites-available/default
    sed -i "s|server_domain_or_IP|$HOST_NAME|"  /etc/nginx/sites-available/default

    useradd nginx
    cp system/nginx_nginx.conf /etc/nginx/nginx.conf

    cp system/nginx_conf.d_10-rstudio.conf /etc/nginx/conf.d/10-rstudio.conf
    sed -i "s|example.com|$HOST_NAME|" /etc/nginx/conf.d/10-rstudio.conf

    systemctl restart nginx
    systemctl enable nginx
#fi

# Docker volumes

# mydatalocal: from the system disk or ephemeral  one
IFB_DATADIR="/ifb/data/"
source /etc/profile.d/ifb.sh
VOL_NAME="mydatalocal"
VOL_DEV=$(readlink -f -n $IFB_DATADIR/$VOL_NAME )
DOCK_VOL=" --mount type=bind,src=$VOL_DEV,dst=$IFB_DATADIR/$VOL_NAME"

# MBB Workflows reads data from /Data and write results to /Results
mkdir ${VOL_DEV}/Data
mkdir ${VOL_DEV}/Results
DOCK_VOL+=" --mount type=bind,src=$VOL_DEV/Data,dst=/Data"
DOCK_VOL+=" --mount type=bind,src=$VOL_DEV/Results,dst=/Results"

# NFS mounts: from ifb_share configuration in autofs
IFS_ORI=$IFS
while IFS=" :" read VOL_NAME VOL_TYPE VOL_IP VOL_DEV ; do
        DOCK_VOL+=" --mount type=volume,volume-driver=local,volume-opt=type=nfs,src=$VOL_NAME,dst=$IFB_DATADIR/$VOL_NAME,volume-opt=device=:$VOL_DEV,volume-opt=o=addr=$VOL_IP"
done < /etc/auto.ifb_share
IFS=$IFS_ORI



CONTAINER_ID=$( docker run -d -p $APP_PORT:3838 $DOCK_VOL $APP_IMG )

VM_IP=$(curl bot.whatismyipaddress.com)

if [ $CONTAINER_ID ]
then
    echo " "
    echo You have to put your Data on  : ${VOL_DEV}/Data
    echo " "
    echo Results will be written to    : ${VOL_DEV}/Results
    echo " "
    echo You can access the workflow interface at :  https://${VM_IP}
    echo " "
    echo To start a Bash session inside the container : docker exec -it $CONTAINER_ID /bin/bash
    echo " "
    echo To run the workflow without the interface : docker exec -it $CONTAINER_ID snakemake -s /workflow/Snakefile all --configfile config --cores XX
    echo " "
    echo config est un fichier de configuration qui doit être dans un sous dossier de ${VOL_DEV}/Data ou ${VOL_DEV}/Results
    echo " "
    echo ex. si fichier dans ${VOL_DEV}/Data/run1/maconfig1.yml : docker exec -it $CONTAINER_ID snakemake -s /workflow/Snakefile all --configfile /Data/run1/maconfig1.yml --cores XX
    echo " "
    echo Vous pouvez utiliser l''interface graphique pour générer un fichier de configuration.
    echo " "
    echo XX étant le nombre de coeurs qui seront utilisés par le workflow.
else
    echo Failed to run the docker container  !!
fi
