This workflow is a snakemake wrapper for Stacks denovo workflow :

![alt text](./files/workflow.png "Workflow")

## 1/ Utilisation du workflow de manière autonome

* Tous les workflows développés par MBB sont disponibles au téléchargement pour un usage personnel autonome sous forme de :
  * Code source hébergé dans un dépôt Git :   <https://gitlab.mbb.univ-montp2.fr/mmassaviol/wapps>
  * Images conteneurisées Docker (outil de conteneurisation) dans le dépôt dockerHub : <https://hub.docker.com/search?q=mbbteam&type=image>

    Learn Docker in 12 Minutes : <https://www.youtube.com/watch?v=YFl2mCHdv24>

* Dans ce qui suit nous allons voir comment déployer un workflow sur une machine autonome, de lancer un workflow en mode Web, de le modifier puis de le lancer en mode ligne de commande.

    * cloner le dépôt Git du radseqdenovo

    * ```git clone https://gitlab.mbb.univ-montp2.fr/khalid/radseqdenovo.git```
    * ```cd radseqdenovo```
    * ```ls -al```

  * Remarquer la présence des fichiers suivants :
    * install.sh permet d'installer les logiciels pré-requis (pas besoin ici !)

    * deployBigMem.sh : permet de déployer un conteneur en mode web sur une machine de type bigmem

    * deployIFB.sh : permet de deployer en mode web sur le cloud IFB (<https://biosphere.france-bioinformatique.fr/cloudweb/login/)>

    * deployLocal.sh : permet de deployer sur votre propre machine ou serveur

    * waw_workflow.qsub : script de soumission d'un workflow sur le cluster MBB

    * RunCmdLine.sh : permet de déployer et executer un workflow en ligne de commande (Nous verrons cela dans une prochaine partie)

  * Lancer ***bash deployLocal.sh*** pour voir les paramètres dont il a besoin :

    * dataDir : dossier de la machine hôte contenant les données

    * resultsDir : dossier de la machine hôte qui va contenir les résultats de l'analyse

    * le dernier paramètre (optionnel) indique la source de l'image Docker à utiliser
      * dockerHub : l'image sera téléchargée depuis le dépôt Docker Hub (par défaut) à éviter car les images ne sont pas à jour des dernières modifications
      * local : l'image sera construite en local à partir des fichiers sources issus du dépôt Git que vous venez de télécharger

    * Pour cette partie, on va supposer que vos données sont disponibles dans le dossier
      /home/$USER/datasets/radseq

    * Créer un dossier pour les résultas ex. : ***mkdir /home/$USER/resultrad***

    * lancer :
        ***deployLocal.sh /home/$USER/datasets/radseq /home/$USER/resultrad local***

    * Voir plus bas pour la correspondance entre chemins sur le système hôte et chemins du conteneur

    * Consulter la sortie écran pour voir comment :
      * Accéder au workflow par navigateur web :
        normalement un lien vers une URL du type 127.0.0.1:8787 à ouvrir dans un navigateur 

      * Accéder en mode *ssh* à l'intérieur du système du conteneur : sudo docker exec -it  <IDduconteneur> /bin/bash

    * Pour arrêter le conteneur :
      * ***docker ps*** pour lister les conteneurs et repérer le bon
      * ***docker kill ID*** éviter de tuer d'autres conteneurs !

  * Modifications du workflow

    ### A/ Ajouter un paramètre à un outil

    Les règles des différentes étapes du workflow sont assemblées dans le fichier  files/Snakefile. Elles sont écrites selon la syntaxe du gestionnaire de workflow Snakemake (<https://github.com/deto/Snakemake_Tutorial)> (<https://www.youtube.com/watch?v=UOKxta3061g&feature=youtu.be)>
    * Ajout du paramètre --phylip-var au module populations pour  'include variable sites in the phylip output encoded using IUPAC notation' :
      * Ouvrir le fichier Snakemake et aller au rule populations
      * Repérer la partie shell qui indique comment sera lancé l'outil populations
      * Insérer le paramètre "--phylip-var " à la place ou après "--phylip "

        * Relancer le conteneur avec l'option 'local' pour reconstruire l'image avec vos modifs

          ***deployLocal.sh /home/$USER/datasets/radseq /home/$USER/resultrad local***

    ### B/ Changer la version d'un outil

    Les procédures d'installation des différente outils nécessaires au bon fonctionnement du workflow sont rassemblées dans un fichier de recette nommé Dockerfile.
    * Ouvrir ce fichier et repérer la partie concernant l'installation de fastqc
    * Modifier le n° de version pour une version de fastqc plus récente ex:
    changer wget http://catchenlab.life.illinois.edu/stacks/source/stacks-2.5.tar.gz \
    en wget  https://www.bioinformatics.babraham.ac.uk/projects/fastqc/fastqc_v0.11.9.zip \
    * Relancer le conteneur avec l'option 'local' pour reconstruire l'image avec vos modifs

      ***deployLocal.sh /home/$USER/datasets/radseq /home/$USER/resultrad local***

    ### C/ Ajouter une étape

    * Deux possibilités :
      * Faire une demande via le système de tickets : <https://kimura.univ-montp2.fr/calcul/helpdesk_NewTicket.html>
      * Demander une formation spécifique sur l'environnement de conception de workflows de MBB

## 2/ Utilisation autonome en mode ligne de commande

Pour ré-utiliser un workflow sur différents fichiers ou avec différents paramètres, il est bon de pouvoir le lancer en ligne de commande.

Il faut pour cela :

* Disposer d'une copie du dépôt Git du workflow (voir [1/](#1-utilisation-dun-workflow-de-mani%c3%a8re-autonome) )
* Avoir un fichier texte contenant tous les paramètres du workflow.

  Ce fichier peut être :
  * Récupéré depuis l'interface Web d'un déploiement comme en [1/](#1-utilisation-dun-workflow-de-mani%c3%a8re-autonome) puis modifié à vos besoins
  * Récupéré depuis le dossier de résultats d'une analyse effectuée avec ce workflow
  * Directement à partir du modèle par défaut disponible dans files/params.total.yml

* Modifier un ou plusieurs paramètres parmi les suivants :
  * results_dir:
  * sample_dir:
  * group_file:
  Changer par exemple le nom des enzymes de restriction
  *   process_radtags_enzyme_1_PE: aciI
      process_radtags_enzyme_2_PE: ''
  ou
  #le filtre sur la valeur minimale de mapq pour gstacks
  * gstacks_min_mapq: 20 

* Enregistrer vos modifs dans maconfig.yaml dans par ex. /media/bigvol/$USER/resultrad/version2/ et sera visible dans le conteneur sous /Result/maconfig.yaml

  * lancer depuis une console la ligne de commande :

    ***bash RunCmdLine.sh /home/$USER/datasets/radseq/ /home/$USER/resultrad/version2/ /Results/maconfig.yaml 10***

  * Suivre la progression du workflow

  * A la fin vérifier le contenu de /home/$USER/resultrad/version2/


## 3/ Correspondance entre dossiers de votre machine et dossiers du conteneur

ex 1 deploiement : ***bash deployLocal.sh /media/bigvol/votrelogin/data1/ /home/$USER/results1/***

A l'intérieur du conteneur :

* /media/bigvol/votrelogin/data1/ -> /Data/
* /home/$USER/results1/ -> /Results/

ex 2 deploiement : ***bash deployLocal.sh /share/bio/datasets/radseq/ /home/$USER/results1/version1/***

A l'interieur du conteneur :

* /share/bio/datasets/radseq/ -> /Data/
* /share/bio/datasets/radseq/fastqs/ -> /Data/fastqs/
* /share/bio/datasets/radseq/reference/ -> /Data/reference/
* /share/bio/datasets/radseq/conditions/groups.tsv -> /Results/conditions/groups.tsv
* /home/$USER/results1/version1/ -> /Results/

## 4/ Liens utiles

* Stacks software :  <http://catchenlab.life.illinois.edu/stacks/>
* commandes docker : <https://devhints.io/docker>
* commandes bash : <https://devhints.io/bash>
* système de tickets MBB : <https://kimura.univ-montp2.fr/calcul/helpdesk_NewTicket.html>
* système de réservation de Bigmem : <https://mbb.univ-montp2.fr/grr/login.php>
* cloud IFB : <https://biosphere.france-bioinformatique.fr/>
* cluster mbb : ssh login@cluster-mbb.mbb.univ-montp2.fr
* depôts Git des MBBworkflows : <https://gitlab.mbb.univ-montp2.fr/mmassaviol/wapps>
* dépôt Git du framework de conception MBBworkflows : <https://gitlab.mbb.univ-montp2.fr/mmassaviol/waw>
* les conteneurs docker des MBBworkflows : <https://hub.docker.com/search?q=mbbteam&type=image>
