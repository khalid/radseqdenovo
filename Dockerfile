FROM mbbteam/mbb_workflows_base:latest as alltools
RUN apt-get update
RUN apt install -y fastqc=0.11.5+dfsg-6

RUN cd /opt/biotools \
 && wget http://catchenlab.life.illinois.edu/stacks/source/stacks-2.5.tar.gz \
 && tar -zxvf stacks-2.5.tar.gz \
 && cd stacks-2.5/ \
 && ./configure \
 && make -j 10 \
 && make install \
 && mv -t ../bin sstacks kmer_filter gstacks tsv2bam process_shortreads populations ustacks phasedstacks cstacks process_radtags \
 && cd .. && rm -r stacks-2.5 stacks-2.5.tar.gz

RUN Rscript -e 'install.packages("calibrate",repos="https://cloud.r-project.org/",Ncpus=8, clean=TRUE);library("calibrate")'

RUN Rscript -e 'BiocManager::install("SNPRelate", version = "3.8",Ncpus=8, clean=TRUE);library("SNPRelate")'
RUN apt-get install -y libcairo2-dev libxt-dev
RUN Rscript -e 'library(devtools);install_github("jokergoo/ComplexHeatmap");library("ComplexHeatmap")'

ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8

#This part is necessary to run on ISEM cluster
RUN mkdir -p /share/apps/bin \
 && mkdir -p /share/apps/lib \
 && mkdir -p /share/apps/gridengine \
 && mkdir -p /share/bio \
 && mkdir -p /opt/gridengine \
 && mkdir -p /export/scrach \
 && mkdir -p /usr/lib64 \
 && ln -s /bin/bash /bin/mbb_bash \
 && ln -s /bin/bash /bin/isem_bash \
 && /usr/sbin/groupadd --system --gid 400 sge \
 && /usr/sbin/useradd --system --uid 400 --gid 400 -c GridEngine --shell /bin/true --home /opt/gridengine sge

EXPOSE 3838
CMD ["Rscript", "-e", "setwd('/sagApp/'); shiny::runApp('/sagApp/app.R',port=3838 , host='0.0.0.0')"]


FROM alltools

COPY files /workflow
COPY sagApp /sagApp

