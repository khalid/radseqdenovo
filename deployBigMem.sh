#!/bin/bash
#This script will help a deployment of a docker image on an MBB bigmem machine

if [ $# -lt 2 ]
then
  echo usage : $0 dataDir resultsDir '[dockerHub|local]'
  exit
fi
#nginx 
##### nginx install #####
#sudo apt-get install -y nginx

    # HOST_NAME="192.168.100.49"

    # HTTP_ENDP="https://$HOST_NAME"

    # openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/ssl/private/nginx-selfsigned.key -out /etc/ssl/certs/nginx-selfsigned.crt -subj "/C=FR/ST=LR/L=Montpellier/O=CNRS/OU=CNRS-ISEM/CN=mbb.univ-montp2.fr"
    # openssl dhparam -out /etc/ssl/certs/dhparam.pem 2048

    # mkdir -p /etc/nginx/snippets
    # echo "ssl_certificate /etc/ssl/certs/nginx-selfsigned.crt;" > /etc/nginx/snippets/self-signed.conf
    # echo "ssl_certificate_key /etc/ssl/private/nginx-selfsigned.key;" >> /etc/nginx/snippets/self-signed.conf

    # cp system/nginx_snippets_ssl-params.conf /etc/nginx/snippets/ssl-params.conf

    # cp /etc/nginx/sites-available/default /etc/nginx/sites-available/default.bak

    # cp system/nginx_sites-available_default /etc/nginx/sites-available/default
    # sed -i "s|server_domain_or_IP|$HOST_NAME|"  /etc/nginx/sites-available/default

    # useradd nginx
    # cp system/nginx_nginx.conf /etc/nginx/nginx.conf

    # cp system/nginx_conf.d_10-rstudio.conf /etc/nginx/conf.d/10-rstudio.conf
    # sed -i "s|example.com|$HOST_NAME|" /etc/nginx/conf.d/10-rstudio.conf

    # systemctl restart nginx
    # systemctl enable nginx

#essayer une plage de ports entre 8787 et 8800
#APP_PORT=$2 
APP_PORT=8787
while [[ $(ss -tulw | grep $APP_PORT) != "" && $APP_PORT < 8800 ]]
do
  APP_PORT=$(( $APP_PORT + 1))
done

if [[ $(ss -tulw | grep $APP_PORT) != "" ]]
then
  echo "No tcp port available !!"
  exit -1
fi

# Docker volumes
# MBB Workflows reads data from /Data and write results to /Results

if [ $SUDO_USER ]; then realUSER=$SUDO_USER; else realUSER=`whoami`; fi

Data=$1
Results=$2

mkdir -p $Data
mkdir -p $Results

DOCK_VOL+=" --mount type=bind,src=$Data,dst=/Data"
DOCK_VOL+=" --mount type=bind,src=$Results,dst=/Results"

if [ $# -lt 3 ]
then
    APP_IMG="mbbteam/radseqdenovo:latest"  
else 
    IMG_SRC=$3
    case $IMG_SRC in
        dockerHub )
            APP_IMG="mbbteam/radseqdenovo:latest"  ;;
        local)
            docker build . -t radseqdenovo:latest 
            APP_IMG="radseqdenovo:latest" ;;
        mbb)    
            #APP_IMG="X.X.X.X:5000/radseqdenovo:latest" ;;
    esac
fi

CONTAINER_ID=$( docker run --rm -d -p $APP_PORT:3838 $DOCK_VOL $APP_IMG )
if [ $CONTAINER_ID ]
then
    echo " "
    echo You have to put your Data on  : $1
    echo " "
    echo Results will be written to    : $2
    echo " "
    hostname -I | grep -E -o "162.38.181.[0-9]{1,3}" | awk -v port=$APP_PORT '{print "You can access the workflow interface at :  http://"$1":"port}'
    echo " "
    echo To start a Bash session inside the container : docker exec -it $CONTAINER_ID /bin/bash
else
    echo Failed to run the docker container  !!
fi
